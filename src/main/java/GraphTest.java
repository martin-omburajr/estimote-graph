import org.jgrapht.Graph;
import org.jgrapht.alg.BellmanFordShortestPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.SimpleGraph;

import java.util.List;

/**
 * Created by martinomburajr on 9/11/2017.
 */
public class GraphTest {
    public GraphTest() {

    }

    public static Graph test() {

        Store triangle = new Store("Triangle");
        triangle.createVertex();
        Store rectangle = new Store("Rectangle");
        Store circle = new Store("Circle");
        Store octagon = new Store("Octagon");
        Store hexagon = new Store("Hexagon");
        Store cross = new Store("Cross");

        Path triangleCross = new Path(triangle,cross);
        Path triangleCircle = new Path(triangle,circle);
        Path circleHexagon = new Path(circle,hexagon);
        Path hexagonOctagon = new Path(hexagon,octagon);
        Path hexagonRectangle = new Path(hexagon,rectangle);
        Path rectangleCross = new Path(rectangle, cross);

        Graph x = new SimpleGraph<Store,Path>(triangleCross.createEdge(triangle, rectangle));


        x.addVertex(triangle);
        x.addVertex(cross);
        x.addVertex(rectangle);
        x.addVertex(hexagon);
        x.addVertex(circle);
        x.addVertex(octagon);

        x.addEdge(triangle,cross);
        x.addEdge(triangle,circle);
        x.addEdge(circle,hexagon);
        x.addEdge(hexagon,octagon);
        x.addEdge(hexagon,rectangle);
        x.addEdge(rectangle,cross);

        BellmanFordShortestPath bellmanFordShortestPath = new BellmanFordShortestPath<Store,Path>(x,triangle);
        DijkstraShortestPath shortestPath = new DijkstraShortestPath<Store,Path>(x,triangle,octagon);
        List<Path> bellPaths = bellmanFordShortestPath.getPathEdgeList(octagon);

        List<Path> paths = shortestPath.getPathEdgeList();
        System.out.println(paths);
        System.out.println(bellPaths );
        return x;
    }

    public static void main(String args[]) {
        Graph g = GraphTest.test();
        System.out.println(g.toString());


    }
}